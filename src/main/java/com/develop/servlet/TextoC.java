package com.develop.servlet;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class TextoC
 */
@WebServlet("/TextoC")
public class TextoC extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TextoC() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
		
	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
		
		
		String boton  = request.getParameter("boton");
		String texto = request.getParameter("textoEscrito");
		String nombre = request.getParameter("nombreArchivo");
		String ruta =request.getParameter("ruta");
		
		File archivo = new File(ruta+"/"+ nombre + ".txt");		
		RequestDispatcher rd;
	
		if(boton.equals("guardar")) {
			boolean band = archivo.createNewFile();
			FileWriter fileWriter = new FileWriter(archivo);
			fileWriter.write(request.getParameter("textoEscrito"));
			fileWriter.close();
			
			if(band){
				System.out.println("se creo archivo");
			}
			
			rd = request.getRequestDispatcher("EscribirTexto.jsp");
			rd.forward(request, response);
			
		
		}else if(boton.equals("leer")) {
			System.out.println("estamos en leer");
			if(archivo.exists()) {
				String cont = "";
				System.out.println("el archivo existe");
				
				Scanner esc = new Scanner(archivo);
				//System.out.println("El archivo contiene esto");
				while(esc.hasNext()) {
					//System.out.println(esc.nextLine());
					cont += esc.nextLine();
					
					}

				FileWriter fileWriter = new FileWriter(archivo);
				fileWriter.write(request.getParameter("textoEscrito"));
				fileWriter.close();
				
				System.out.println("se edito el archivo");
				
				
				
				
				rd = request.getRequestDispatcher("/EscribirTexto.jsp");
				rd.forward(request, response);
				
			}
			
			
			
		}else if(boton.equals("copiar")) {
			
			Files.write(Paths.get(ruta+"/"+ "copyOf  "+ nombre + ".txt"),
				Files.readAllBytes(Paths.get(ruta+"/"+ nombre + ".txt")));
			 System.out.println("se copio el archivo");
				rd = request.getRequestDispatcher("/EscribirTexto.jsp");
				rd.forward(request, response);
			
			
		}
	}

}
